import React, { Component } from 'react';

import Navbar from "./components/Navbar";
import AddUser from "./components/AddUser";
import Users from "./components/Users";
import Graph from "./components/Graph";
//import Test from "./components/Test";
// <Test test="deneme" />
import './App.css';

class App extends Component {
  
  render() {
    return (
      <div className="container">
        <Navbar title="User App" />
        <hr />
        <AddUser/>
        <Users/>
        <Graph/>
      </div>
    )
  }
}

export default App;
