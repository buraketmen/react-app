import React, { Component } from 'react'

class Test extends Component {

    constructor(props) {
        super(props);
        this.state = {
            a:10
        }
        console.log("Constructor");
    }
    componentDidMount() {
        console.log('componentDidMount')
        //API ISTEKLERI
        this.setState({
            a:20
        })
    }
    shouldComponentUpdate() {
        console.log('ShouldComponentUpdate')
        return true;
    }
    componentDidUpdate =(prevProps, prevState) =>{
        console.log('componentDidUpdate')

    }
    
    
    render() {
        console.log("Render")
        return (
            <div>
                
            </div>
        )
    }
}
export default Test;