import React from 'react';
import Proptypes from "prop-types";
import Plotly from "plotly.js";

import createPlotlyComponent from "react-plotly.js/factory";
const Plot = createPlotlyComponent(Plotly);

function Graph(props) {
    return (
        <Plot
            data={[
                {
                    x: [1, 2, 3],
                    y: [2, 6, 3],
                    type: 'scatter',
                    mode: 'lines+markers',
                    marker: { color: 'red' },
                },
                { type: 'bar', x: [1, 2, 3], y: [2, 5, 3] },
            ]}
            layout={{ width: 700, height: 500, title: 'A Fancy Plot' }}
        />
    )
}
Graph.propTypes = {
    title: Proptypes.string.isRequired
}
Graph.defaultProps = {
    title: "Graph"
}

export default Graph;