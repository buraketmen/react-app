import React, { Component } from 'react';
import PropTypes from 'prop-types';
import UserConsumer from "../context";

class User extends Component {
    state = {
        isVisible: false
    }
    static defaultProps = {
        name: "Bilgi Yok",
        salary: "Bilgi Yok",
        department: "Bilgi Yok"
    }

    onClickEvent(number,e){
        this.setState({
            isVisible : !this.state.isVisible
        })
        
    }
    onDeleteUser = (dispatch,e) => {
        const {id} = this.props;
        //consumer dispatch
        dispatch({type:"DELETE_USER",payload:id});
    }
    componentWillUnmount() {
        console.log('Component Will Unmount')
    }
    render() {
        //destructing
        const { name, department, salary } = this.props;
        const {isVisible} = this.state;
        return (<UserConsumer>
            {
                value => {
                    const {dispatch} = value;
                    return (
                        <div className="col-md-8 mb-4" >
                            <div className="card" style={isVisible ? {  backgroundColor: "#62848d",color:"white"}: null}>
                                <div className="card-header d-flex justify-content-between">
                                    <h4 className="d-inline" onClick={this.onClickEvent.bind(this)}>{name}</h4>
                                    <i onClick={this.onDeleteUser.bind(this,dispatch)} style={{ cursor: 'pointer' }}> Sil</i>
                                </div>
                                {
                                    isVisible ?
                                        <div className="card-body">
                                            <div className="card-text">Maaş: {salary}</div>
                                            <div className="card-text">Departman: {department}</div>
                                        </div>
                                        : null
                                }

                            </div>
                        </div>
                    )
                }
            }
        </UserConsumer>)
        
    }
}

User.propTypes = {
    name: PropTypes.string.isRequired,
    salary: PropTypes.string.isRequired,
    department:PropTypes.string.isRequired,
    id: PropTypes.string.isRequired
}
export default User;